const update_btn = document.getElementById('update');
const wb_href = document.getElementById('wb-href');
const table_wrapper = document.getElementById('table-wrapper');

const supplier_id_td = document.getElementById('supplier-id');

const access_token_td = document.getElementById('access-token');
const expires_access_td = document.getElementById('expires-access-token');

const refresh_token_td = document.getElementById('refresh-token');
const expires_refresh_td = document.getElementById('expires-refresh-token');
let hostname = '';

(async function initPopupWindow() {
    let [tab] = await chrome.tabs.query({active: true, currentWindow: true});

    if (tab?.url) {
        try {
            chrome.cookies.get({
                    url: "https://seller.wildberries.ru/",
                    name: "x-supplier-id"
                },
                function (ans) {
                    supplier_id_td.innerHTML = ans.value;
                });
        } catch {
            //
        }

        try {
            chrome.cookies.get({
                    url: "https://passport.wildberries.ru/",
                    name: "WBToken"
                },
                function (ans) {
                    refresh_token_td.innerHTML = ans.value;
                    expires_refresh_td.innerHTML = UnixToDate(ans.expirationDate);
                });
            chrome.cookies.get({
                    url: "https://seller.wildberries.ru/",
                    name: "WBToken"
                },
                function (ans) {
                    access_token_td.innerHTML = ans.value;
                    expires_access_td.innerHTML = UnixToDate(ans.expirationDate);
                });
        } catch {
            //
        }

        try {
            let url = new URL(tab.url);
            hostname = url.hostname;
            if (hostname === "seller.wildberries.ru") {
                wb_href.classList.add('display-none');
                update_btn.classList.remove('display-none');
                table_wrapper.classList.remove('display-none');
            } else {
                update_btn.classList.add('display-none');
                table_wrapper.classList.add('display-none');
                wb_href.classList.remove('display-none');
            }
        } catch {
            //
        }
    }

})();

update_btn.addEventListener("click", function () {
    chrome.cookies.remove({
        url: "https://seller.wildberries.ru/",
        name: "WBToken",
    });
    chrome.tabs.reload();
})

chrome.cookies.onChanged.addListener(function (changeInfo) {
    if (changeInfo.cookie.name === 'WBToken' &&
        changeInfo.cookie.domain === 'passport.wildberries.ru' &&
        changeInfo.cause === 'explicit' &&
        changeInfo.removed === false
    ) {
        console.log(changeInfo)
        refresh_token_td.innerHTML = changeInfo.cookie.value;
        expires_refresh_td.innerHTML = UnixToDate(changeInfo.cookie.expirationDate);

    }

    if (changeInfo.cookie.name === 'WBToken' &&
        changeInfo.cookie.domain === 'seller.wildberries.ru' &&
        changeInfo.cause === 'explicit' &&
        changeInfo.removed === false
    ) {
        console.log(changeInfo)
        access_token_td.innerHTML = changeInfo.cookie.value;
        expires_access_td.innerHTML = UnixToDate(changeInfo.cookie.expirationDate);
    }

})

function UnixToDate(timestamp) {
    let date = new Date(timestamp * 1000),
        year = date.getFullYear(),
        month = date.getMonth() + 1,
        day = date.getDate(),
        hours = date.getHours(),
        minutes = date.getMinutes(),
        seconds = date.getSeconds();

    month = month < 10 ? "0" + month : month;
    day = day < 10 ? "0" + day : day;
    hours = hours < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    return year + "-" + month + "-" + day + "  " + hours + ":" + minutes + ":" + seconds;
}